# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="afowler"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
#ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(asdf command-not-found colored-man-pages git ubuntu vi-mode)

fpath=($HOME/.homesick/repos/homeshick/completions $fpath)

source $ZSH/oh-my-zsh.sh

## ALIASES
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias diff="diff -y --suppress-common-lines"
# Git
alias gc="git commit -v"
alias gca="git commit -va"
alias gco="git checkout"
alias gl="git pull --prune"
alias glr="git pull --prune --rebase"
alias gp="git push origin HEAD"
alias gst="git status -s"

alias clean-exif="exiftool -all="
alias reload="source ~/.zshrc"
alias ws="cd $HOME/Work"

### EXPORTS
export CHECKPOINT_DISABLE=1
export EDITOR="vim"
export FZF_CTRL_R_OPTS="--sort"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$HOME/.local/bin:$PATH"
#export GOPATH="$HOME/.go"
export MANPAGER="less -X"
export POWERLINE_CONFIG_COMMAND=~/.local/bin/powerline-config

# Perform ls after each dir-change
# http://stackoverflow.com/questions/3964068/zsh-automatically-run-ls-after-every-cd
function chpwd() {
    emulate -L zsh
    ls
}

### HISTORY
export HISTSIZE=32768
export SAVEHIST=32768
# Save command's timestamp and duration.
setopt EXTENDED_HISTORY
# Write to the history file immediately, not when the shell exits.
setopt INC_APPEND_HISTORY
# No bell on error in history
#unsetopt HIST_BEEP
# Expire duplicate entries first when trimming history.
setopt HIST_EXPIRE_DUPS_FIRST
# Do not display a line previously found.
setopt HIST_FIND_NO_DUPS
# Delete old recorded entry if new entry is a duplicate.
setopt HIST_IGNORE_ALL_DUPS
# Don't record an entry that was just recorded again.
setopt HIST_IGNORE_DUPS
# Don't record an entry starting with a space.
setopt HIST_IGNORE_SPACE
# Remove superfluous blanks before recording entry.
setopt HIST_REDUCE_BLANKS
# Don't write duplicate entries in the history file.
setopt HIST_SAVE_NO_DUPS
# Share history between all sessions.
setopt SHARE_HISTORY

# Change into a dir if command given is the name of a dir in pwd.
setopt AUTO_CD
#setopt ALWAYS_TO_END
# Treat the `#', `~' and `^' characters as part of patterns for filename generation, etc.
setopt EXTENDED_GLOB
# Automatically list choices on an ambiguous completion.
setopt LIST_TYPES
# Bash-like globbing
setopt NONOMATCH

# gpg-agent
export GPG_TTY="$(tty)"
#gpgconf --launch gpg-agent
gpg-connect-agent /bye
#gpg-connect-agent updatestartuptty /bye
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

source ~/.homesick/repos/homeshick/homeshick.sh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/autojump/autojump.sh
source /usr/share/doc/fzf/examples/completion.zsh
source /usr/share/doc/fzf/examples/key-bindings.zsh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

eval "$(direnv hook zsh)"
