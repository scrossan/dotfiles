execute pathogen#infect()

" Fzf fuzzy matching
source /usr/share/doc/fzf/examples/fzf.vim

" Powerline
set rtp+=/home/stevencrossan/.local/lib/python3.9/site-packages/powerline/bindings/vim
set t_Co=256

" Syntax Checking
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_loc_list_height = 5
let g:syntastic_python_checkers = ["flake8"]
let g:syntastic_python_flake8_post_args = "--ignore=E501"

" Editorconfig
let g:EditorConfig_core_mode = "external_command"
let g:EditorConfig_exec_path = "/usr/bin/editorconfig"
let g:EditorConfig_exclude_patterns = ["fugitive://.*"]

colorscheme molokai
set background=dark
set termguicolors
set shiftwidth=2

syntax enable
set tabstop=2
set softtabstop=2
set expandtab
set autoindent
set backspace=indent,eol,start
set shiftwidth=2
set number

set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set list

set showcmd
"set cursorline
filetype indent on
filetype plugin on
set wildmenu
set lazyredraw
set showmatch
set laststatus=2
set showtabline=2
set noshowmode
set ruler

set incsearch
set hlsearch
set ignorecase
set report=0

set nocompatible
set foldenable
set foldlevelstart=10
"set foldnestmax=10
nnoremap <space> za
set foldmethod=indent

set backup
set backupdir=~/.vim/backups
set directory=~/.vim/swaps
set undodir=~/.vim/undo
set writebackup

autocmd filetype crontab setlocal nobackup nowritebackup
